#

variables:
  BLUOS_CONTROLLER_VERSION: "4.8.0"
  BLUOS_INSTALLER_EXE: "BluOS Controller ${BLUOS_CONTROLLER_VERSION} Windows.exe"
  BLUOS_INSTALLER_ZIP: "BluOS-Controller-${BLUOS_CONTROLLER_VERSION}-Windows.zip"
  BLUOS_INSTALLER_URL: "https://bluos.io/wp-content/uploads/2025/01/${BLUOS_INSTALLER_ZIP}"

  # the generated image
  APP_IMAGE: bluos-controller-linux-${BLUOS_CONTROLLER_VERSION}.AppImage

default:
  before_script:
    - env | egrep "^CI|^BLUOS" | sort

app-image:
  stage: build
  image: node:current
  variables:
    TMP_DIR: ${CI_PROJECT_DIR}/tmp
    PATCH: ${CI_PROJECT_DIR}/patches/${BLUOS_CONTROLLER_VERSION}-linux.patch
  script:
    # install dependencies
    - apt-get update && apt-get install -y --no-install-recommends p7zip
    - corepack enable
    - echo y | yarn add asar js-beautify npx --dev

    # download and unzip
    - wget -q ${BLUOS_INSTALLER_URL}
    - unzip "${BLUOS_INSTALLER_ZIP}"
    - 7zr x "${BLUOS_INSTALLER_EXE}" -o${TMP_DIR}

    # extract app.asar
    - cd $TMP_DIR
    - npx asar extract resources/app.asar app/
    # beautify needed before patching
    - find . -ls
    - find app/packages -name "index*.js"
    - find app/packages -name "index*.js" | xargs -n 1 npx js-beautify -r index-*.js
    - cd app/
    - cat ${PATCH}
    - patch -p1 < ${PATCH}
    # the created AppImage would not start without this
    - mkdir resources
    - cp -av ../resources/analytics resources/
    # icon
    - mkdir icons && cp -v packages/renderer/dist/img/icon.png icons/256x256.png
    # build AppImage
    - yarn
    - yarn run electron-builder -l AppImage
    - ls -l dist
    - cp dist/*-${BLUOS_CONTROLLER_VERSION}*.AppImage ${CI_PROJECT_DIR}/${APP_IMAGE}

  artifacts:
    paths:
      - ${CI_PROJECT_DIR}/*${BLUOS_CONTROLLER_VERSION}*.AppImage

# publish a generic package
package:
  stage: deploy
  script:
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN"
      --upload-file ${APP_IMAGE}
      "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/bluos-controller-linux/${BLUOS_CONTROLLER_VERSION}/${APPIMAGE}"'
  only:
    - tags
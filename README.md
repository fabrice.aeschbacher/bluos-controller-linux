# bluos-controller-linux

The **BluOS Controller Application** from
[bluos.net](https://bluos.net/downloads/)
lacks a Linux version.

There seems to be a
[wish](https://support1.bluesound.com/hc/en-us/community/posts/360033533054-BluOS-controller-app-on-Linux)
 from the user community though for a Linux version.

As it is an [electron](https://www.electronjs.org/)
application, building a version for Linux is possible.

This repository provides a build pipeline which builds a Linux version
by patching the files released on https://bluos.net/downloads .

----
Credits:
- The original patch idea is from
  [David S](https://support1.bluesound.com/hc/en-us/profiles/411622135234-David-S)
- Multiple ideas gathered at [support1.bluesound.com](https://support1.bluesound.com/hc/en-us/community/posts/360033533054-BluOS-controller-app-on-Linux)